const Stats = require("../entities/Stats");

const getStats = (statsRepository) => async () => {
  const statsEntity = new Stats(statsRepository);

  try {
    const normalStats = await statsEntity.get(false);
    const mutatedStats = await statsEntity.get(true);
    const ratio = mutatedStats / normalStats;

    return {
      statusCode: 200,
      body: JSON.stringify({
        count_mutations: mutatedStats,
        count_no_mutation: normalStats,
        ratio: ratio,
      }),
    };
  } catch (e) {
    return {
      error: e.message,
    };
  }
};

module.exports = getStats;
