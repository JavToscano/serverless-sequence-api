const Sequence = require("../entities/Sequence");
const mutationHelper = require("../helpers/hasMutation");

const addSequence = (sequenceRepository) => async (dna) => {
  const sequenceEntity = new Sequence(sequenceRepository);

  try {
    const mutation = mutationHelper.hasMutation(dna);
    await sequenceEntity.create(dna, mutation);

    let statusCode = 403;
    if (mutation) statusCode = 200;

    return { statusCode: statusCode };
  } catch (e) {
    console.log(e);
    return {
      error: e.message,
    };
  }
};

module.exports = addSequence;
