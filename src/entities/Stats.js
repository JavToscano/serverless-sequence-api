class Stats {
  constructor(repository) {
    this.repository = repository;
  }

  get(hasMutation) {
    return this.repository.get(hasMutation);
  }
}

module.exports = Stats;
