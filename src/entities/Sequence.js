class Sequence {
  constructor(repository) {
    this.repository = repository;
  }

  create(dna, mutation) {
    return this.repository.create(dna, mutation);
  }
}

module.exports = Sequence;
