const uuid = require("uuid");

class DynamoDBRepository {
  constructor(client, table) {
    this.client = client;
    this.table = table;
  }

  async get(hasMutation) {
    const { Count } = await this.client
      .scan({
        TableName: this.table,
        FilterExpression: "hasMutation = :mutation",
        ExpressionAttributeValues: {
          ":mutation": hasMutation,
        },
      })
      .promise();
    return Count;
  }

  async create(sequence, hasMutation) {
    await this.client
      .put({
        TableName: this.table,
        Item: {
          id: uuid.v4(),
          sequence: sequence,
          hasMutation: hasMutation,
        },
      })
      .promise();
  }
}

module.exports = DynamoDBRepository;
