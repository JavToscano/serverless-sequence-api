const AWS = require("aws-sdk");
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const DynamoDBRepository = require("../repositories/dynamodb");
const sequenceRepository = new DynamoDBRepository(dynamoDb, "dna-sequence");
const addSequenceUseCase = require("../usecases/add-sequence")(
  sequenceRepository
);

const handler = async ({ body }) => {
  try {
    const { dna } = JSON.parse(body);
    return addSequenceUseCase(dna);
  } catch (e) {
    return { statusCode: 403 };
  }
};

module.exports = { handler };
