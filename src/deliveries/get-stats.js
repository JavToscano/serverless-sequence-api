const AWS = require("aws-sdk");
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const DynamoDBRepository = require("../repositories/dynamodb");
const statsRepository = new DynamoDBRepository(dynamoDb, "dna-sequence");
const getStatsUseCase = require("../usecases/get-stats")(statsRepository);

const handler = async () => {
  return getStatsUseCase();
};

module.exports = { handler };
