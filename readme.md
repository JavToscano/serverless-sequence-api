# Serverless DNA Mutation 

Serverless DNA Mutation verifier using AWS Lambda and DynamoDB for auto scaling.

For testing on AWS cloud please use the following endpoints:

```sh
  GET - https://r8ft9f5a6i.execute-api.us-east-1.amazonaws.com/production/stats
  POST - https://r8ft9f5a6i.execute-api.us-east-1.amazonaws.com/production/mutation
```

## Installation

For local testing clone the repo and execute on terminal the following commands

```sh
npm install
npm start 
```

this is going to emulate AWS lambda offline for local testing.

## Deployment

To deploy the proyect to AWS Cloud you can do it via code or gitlab CI/CD hooks.

```sh
npm run deploy
```

## Testing

Unit test can be run on jest using the following command

```sh
npm test
```

