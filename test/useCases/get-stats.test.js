const aws = require("aws-sdk");

jest.mock("aws-sdk", () => {
  const mDocumentClient = { get: jest.fn() };
  const mDynamoDB = { DocumentClient: jest.fn(() => mDocumentClient) };
  return { DynamoDB: mDynamoDB };
});
const mDynamoDb = new aws.DynamoDB.DocumentClient();

const getStatsUseCase = require("../../src/usecases/get-stats")(mDynamoDb);

it("Should get mutation stats", async () => {
  const shouldBe = { count_mutations: 40, count_no_mutation: 100, ratio: 0.4 };
  mDynamoDb.get
    .mockImplementationOnce(() => Promise.resolve(100))
    .mockImplementationOnce(() => Promise.resolve(40));

  const result = await getStatsUseCase();
  const body = JSON.parse(result.body);

  expect(result.statusCode).toBe(200);
  expect(body).toEqual(shouldBe);
});
