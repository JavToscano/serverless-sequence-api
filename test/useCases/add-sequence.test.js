const aws = require("aws-sdk");

jest.mock("aws-sdk", () => {
  const mDocumentClient = { create: jest.fn() };
  const mDynamoDB = { DocumentClient: jest.fn(() => mDocumentClient) };
  return { DynamoDB: mDynamoDB };
});
const mDynamoDb = new aws.DynamoDB.DocumentClient();

const addSequenceUseCase = require("../../src/usecases/add-sequence")(
  mDynamoDb
);

describe("ADN store and mutation validation", () => {
  it("Should add DNA sequence and return statusCode 200", async () => {
    const data = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"];

    const result = await addSequenceUseCase(data);
    expect(result.statusCode).toBe(200);
  });
});
