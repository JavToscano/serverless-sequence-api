const { handler } = require("../../src/deliveries/add-sequence");

describe("Validate handler method", () => {
  it("Should return 403 if dna is not an array", async () => {
    const data = "CCTTT";
    const result = await handler(data);
    expect(result.statusCode).toBe(403);
  });
});
